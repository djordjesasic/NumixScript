#!/bin/bash

#run this script with root privilages
#this script will install git on your system
numix_dir="/home/$1/numix"
git=`command git --version 2>&1 >/dev/null`
icons="/home/$1/.icons"
themes="/home/$1/.themes"

echo "USAGE: "
echo "./numixscript.sh username"

if [[ $EUID -ne 0 ]]; then
	echo "You have to run this shit as root"
	exit 1
fi

if [[ $# -eq 0 ]]; then
	echo "This script requires arguments!"
	sleep 1
	echo "USAGE: ./numixscript.sh username"
	exit 1
fi

echo "NUMIX SETUP"
sleep 1
echo "Checking for git"
if [[ $git -eq 0 ]]; then
	echo "git already installed on this system"
	sleep 1
else
	while true; do
		echo "git is not installed on your system."
		sleep 1
		read -p "would you like to install it now? Y\\n: " yn
		case $yn in
			"Y" | "y" | "" ) apt install git -y; break;;
			"N" | "n" ) exit;;
			* ) echo "Invalid input!";;
		esac
	done
fi

if [ ! -d $numix_dir ]; then
	mkdir $numix_dir
	cd $numix_dir
else
	cd $numix_dir
fi

{
git clone https://github.com/cldx/numix-gtk-theme
git clone https://github.com/numixproject/numix-icon-theme
git clone https://github.com/numixproject/numix-icon-theme-circle
}
if [ ! -d $icons ]; then
	mkdir $icons
fi
if [ ! -d $themes ]; then
	mkdir $themes
fi

cp -R $numix_dir/numix-icon-theme/Numix $icons
cp -R $numix_dir/numix-icon-theme-circle/Numix-Circle $icons
cp -R $numix_dir/numix-gtk-theme $themes
gtk-update-icon-cache /home/$1/$icons/Numix/
gtk-update-icon-cache /home/$1/$icons/Numix-Circle/
echo "DONE!"
